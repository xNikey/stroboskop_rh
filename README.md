# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

git clone https://TheRoktor@bitbucket.org/TheRoktor/stroboskop.git
git rm nepotrebno.js
git add jscolor.js

Naloga 6.2.3:
https://bitbucket.org/TheRoktor/stroboskop/commits/0bc6f80e81ca95129be7a29ee337bb9ee746e9eb

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/TheRoktor/stroboskop/commits/cb74394d2d6b4d04191f61a92e6c2c29dbb2c169

Naloga 6.3.2:
https://bitbucket.org/TheRoktor/stroboskop/commits/c74188daa649238f33b052762dd244200af7d214

Naloga 6.3.3:
https://bitbucket.org/TheRoktor/stroboskop/commits/c050b94b182de3f1b783b9838d85a3828d0949e8

Naloga 6.3.4:
https://bitbucket.org/TheRoktor/stroboskop/commits/24d6e5dd808fd2b2f73870f76441cffdec5feace

Naloga 6.3.5:

git checkout master
git merge izgled
git push origin master

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/TheRoktor/stroboskop/commits/b59d8f36bb51fcee7fc0a96ee523ddc7589b2b9d

Naloga 6.4.2:
https://bitbucket.org/TheRoktor/stroboskop/commits/cf9212870269a880c2aaf4751cf3c394a0d520ed

Naloga 6.4.3:
https://bitbucket.org/TheRoktor/stroboskop/commits/0c8c2147431a6c21f8045facd38ae1e5e1c02a4b

Naloga 6.4.4:
https://bitbucket.org/TheRoktor/stroboskop/commits/33b41f415f9fce7fe95cdbfa6de2920868966a00